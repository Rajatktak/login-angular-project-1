import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageComponent } from './page/page.component';



const routes: Routes = [
  {path:'login',component: LoginComponent,},
  {path:'dashboard', component:DashboardComponent   },
    {path:'signup', component: SignupComponent  },
    {path:'',redirectTo:'/login' , pathMatch:'full'},
    {path:'**',component:PageComponent}
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const  routingComponents = [LoginComponent,SignupComponent,DashboardComponent,PageComponent]
